### SOUPIS VŠECH FUNKCÍ

## Frame pro nový slide
```json

{
    "class": [],
    "zadani": {
      "hlavni": "",
      "rozsirujici": ""
    },
    "napoveda": {
      "aktivity": [""],
      "text": ""
    }
}

```

## Prameny s popisky
```json
{
    "prameny": [
      "co-delaji-zeny-na-fotografii/pic-00-1280w.jpg",
      "co-delaji-zeny-na-fotografii/pic-01-1280w.jpg"
    ],
    "popisky": [
      "Provizorní barikáda, vytvořená z vraků...",
      "Instalace německo-syrského umělce..."
    ]
}

```

## SVG s popisky
```json
{
    "svg": {
        "soubory": [
          {
            "id": "svg-2", // pojmenování od svg-1, svg-2, svg-3
            "soubor": "co-delaji-zeny-na-fotografii/pic-00-1280w.jpg",
            "duplikovat": [
              "svg-1"
            ],
            "funkce": [
              "text",
              "kresleni",
              "znacky"
            ],
            "barvy": ["blue"]
          }
        ]
      }
}

```

## SVG s komiksem

```json
{
    "svg": {
        "nastaveni": {
          "layout": "velka-galerie"
        },
        "soubory": [
          {
            "id": "svg-1",
            "soubor": "co-ministr-vzkazuje-ucitelum/pic-02-1024h.jpg",
            "funkce": [
              "text",
              "komiks"
            ],
            "komiks": [
              {
                "kategorie": "text",
                "subkategorie": "myslenka",
                "pozice": [ //nutné ruční napozicování bubliny
                  60,
                  200
                ],
                "subjekt": [
                  "top",
                  "right"
                ]
              },
              {
                "kategorie": "text",
                "subkategorie": "myslenka",
                "pozice": [ //nutné ruční napozicování bubliny
                  710,
                  230
                ],
                "subjekt": [
                  "top",
                  "left"
                ]
              }
            ]
          }
        ]
      }
}
```

## Klíčové slova (zvětšování + a -)
```json
{
    "klicovaSlova": {
        "klicovaSlova": [
          {
            "id": "klicova-slova-1",
            "funkce": "wordcloud",
            "tagy": [
              {
                "text": "utrpení obyvatel ve válce"
              },
              {
                "text": "vynalézavost lidí v době války"
              }
            ]
          }
        ]
      }
}
```


## Uživatelský text
```json
{
    "uzivatelskyText": {
        "otazky": [
          {
            "id": "text-1", // pojmenování od text-1, text-2, text-3 BUDE SE MĚNIT //TODO
            "zadani": "Napište své hodnocení Halbouniho díla.",
            "instrukce": "Moje hodnocení díla (čím mě zaujalo, co mi na něm vadí, jaké pocity ve mě vyvolává, co mě napadá…)",
            "minDelka": 2,
            "maxDelka": 400
          },
          {
            "id": "text-2",
            "zadani": "Navrhněte místo v České republice, kde by bylo vhodné dílo umístit. Svůj výběr zdůvodněte.",
            "instrukce": "Myslím si, že…",
            "minDelka": 2,
            "maxDelka": 400
          }
        ]
      }
}
```

## Audio & Video
```json
{
    "soubory": [
          {
            "soubor": "co-ministr-vzkazuje-ucitelum/audio-00.mp3",
            "nazev": "Škola má učit, ale i vychovávat socialistického člověka.",
            "popisek": "Projev ministra školství ČSR Jaromíra Hrbka z 1. 9. 1969 k začátku školního roku (kráceno). Zdroj: Archiv ČRo"
          },
          {
            "soubor": "co-ministr-vzkazuje-ucitelum/audio-01.mp3",
            "nazev": "Škola NEmá učit, ale hlavně vychovávat socialistického člověka.",
            "popisek": "Projev ministra školství ČSR Jaromíra Hrbka z 1. 9. 1969 k začátku školního roku (kráceno). Zdroj: Archiv ČRo"
          }
        ]
}
```

## Textový editor (označování pasáží)
```json
{
    "textovyEditor": {
        "texty": [
          {
            "id": 0, //označení pomocí stringu, BUDE SE MĚNIT //TODO
            "text": "text-00",
            "funkce": "zvyraznovani",
            "menu": [
              {
                "title": "Ovlivňuje postoje",
                "commandName": "ovlivnuje-postoje",
                "classes": "text-green",
                "command": "toggle-class",
                "commandOptions": {
                  "toggleClass": "node-selected node-text-green",
                  "closeBtn": true,
                  "comments": false,
                  "commentsClass": "text-orange"
                }
              }
            ]
          }
        ]
      }
}
```

## Řazení se zpětnou vazbou
```json
{
    "razeni": {
        "id": "razeni-1",
        "zpetnaVazba": [
          {
            "podminka": 2, // Dvě a více chyb
            "text": "Opravdu? My jsme fotografie uspořádali odlišně.",
            "barva": "color-red"
          },
          {
            "podminka": 1, // Přesně jedna chyba
            "text": "Jednu fotografii bychom uspořádali odlišně.",
            "barva": "color-orange"
          },
          {
            "podminka": 0, // 0 chyb
            "text": "Výtečně. Uspořádali jsme fotografie stejně.",
            "barva": "color-green"
          }
        ],
        "typ": "horizontalni",
        "objekty": [
          {
            "medium": "audio",
            "objekt": "proc-byli-uneseni/audio-00.mp3",
            "nazev": "Oznámení o zadržení československých občanů v Angole.",
            "spravnaOdpoved": "2"
          },
          {
            "medium": "obrazek",
            "objekt": "dejiny-na-namesti/pic-00-1024h.jpg",
            "popisek": "Zdroj: Archiv DP Praha",
            "spravnaOdpoved": "4"
          }
          {
            "medium": "svg",
            "objekt": {
              "soubor": "co-se-stalo-v-abertamech/pic-03-1024w.jpg",
              "duplikovat": ["svg-3"]
            },
            "spravnaOdpoved": "3"
          },
          {
            "medium": "svg",
            "objekt": {
              "soubor": "co-se-stalo-v-abertamech/pic-02-1024w.jpg",
              "duplikovat": ["svg-4"]
            },
            "spravnaOdpoved": "1"
          }
        ]
      }
}
```