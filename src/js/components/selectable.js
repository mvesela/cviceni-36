/* eslint-disable linebreak-style */
import Component from './component';
import { __dispatchEvent } from '../lib/utils';

export default class Selectable extends Component {

  constructor($element) {
    super($element);

    // TODO: is DOM element before/after the editor itself.. move it to the editor?
    this.$settings = document.querySelector(`[data-selectable-id-reference="${this.target.id}"]`);

    this.feedback = JSON.parse(this.$settings.getAttribute('data-module-zpetnavazba'));
    this.settings = JSON.parse(this.$settings.getAttribute('data-module-settings'));

    this.$items = this.target.querySelectorAll('.selectable-item');

    this.cache = {
      answersCorrect: 0,
      zpetnaVazba: false,
      colors: [],
      checkedInput: 0,
    };

    this.$inputs = this.target.querySelectorAll('input');
    this.$inputs.forEach(($input) => {
      this._changeClassIfChecked($input);
      this._addEventListeners($input);
    });

    // get colors for all feedback variants
    if (this.feedback) {
      this.cache.zpetnaVazba = true;
      this.cache.colors = this.feedback.map((feedback) => feedback.barva);
    }

  }

  _addEventListeners($input) {
    $input.addEventListener('change', () => {

      this.$inputs.forEach(($input_) => {
        this._changeClassIfChecked($input_);
      });

      this._checkAndResolveFeedback();

      this._dispatchEvent($input);
    }, false);
  }

  /* eslint class-methods-use-this: ["error", { "exceptMethods": ["_changeClassIfChecked"] }] */
  // TODO: might be possible with CSS only with `:checked` selector & sibling combinator`+` or `~`
  _changeClassIfChecked($input) {
    $input.closest('.selectable-item').classList.toggle('is-checked', $input.checked === true);
  }

  _checkAndResolveFeedback() {
    this.cache.answersCorrect = 0;

    if (this.settings.viceOdpovedi) {
      this.$items.forEach(($item, index) => {
        if (this.$inputs[index].checked) {
          this.cache.checkedInput += 1;
        }
        if (this.$inputs[index].checked && $item.hasAttribute('data-select-spravnaodpoved')) {
          this.cache.answersCorrect += 1;
        }
        if (!this.$inputs[index].checked && !$item.hasAttribute('data-select-spravnaodpoved')) {
          this.cache.answersCorrect += 1;
        }
      });
    }
    else {
      this.$items.forEach(($item, index) => {
        if (this.$inputs[index].checked) {
          this.cache.checkedInput += 1;
        }
        if (this.$inputs[index].checked && $item.hasAttribute('data-select-spravnaodpoved')) {
          this.cache.answersCorrect += 1;
        }
      });
    }

    // Display feedback button (if no checkbox is select, the button is hidden)
    if ((this.cache.checkedInput === 0 && !this.settings.vybrano) || (this.cache.checkedInput === this.$inputs.length && this.settings.vybrano) || this.settings.disableFeedback) {
      this.hideFeedback();
    } else {

      if (this.cache.zpetnaVazba) {
        const feedbackActual = this.feedback.find((feedback) => feedback.podminka.includes(this.cache.answersCorrect));
        this.$buttonFeedback.classList.remove(...this.cache.colors);
        this.$buttonFeedback.classList.add(feedbackActual.barva);
        this.$buttonFeedbackText.innerText = feedbackActual.text;
      }

      this.showFeedback();
    }

    this.target.classList.toggle('has-checked', this.cache.checkedInput > 0);

    this.cache.checkedInput = 0;
  }

  _dispatchEvent($input) {
    // const items = Array.from(this.$inputs).map(($input) => ({
    //   checked: $input.checked,
    //   id: $input.id,
    // }));
    const $label = $input.parentNode.querySelector('label');
    let task;

    if ($input.type === 'radio') {
      task = 'change';
    }
    else if ($input.type === 'checkbox') {
      task = ($input.checked) ? 'select' : 'unselect';
    }
    else {
      console.error('ERROR: Undefinated input type.');
    }

    __dispatchEvent(this.target, 'selection.change', {}, {
      id: this.target.id,
      task,
      itemId: $input.id,
      itemData: $input.getAttribute('data-selectable-data'),
      $node: $label.firstChild,
    });
  }
}
